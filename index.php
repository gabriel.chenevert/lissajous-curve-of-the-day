<html><?php require "code/lcd.php"; ?>

<head>
 <title>Lissajous Curve of the Day</title>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" type="text/css"href="lcd.css"></link>
 <link rel="icon" type="image/png" href="icon.png"></link>
 <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
</head>

<body>

<h1>Lissajous Curve of the Day <a href="https://twitter.com/CurveDay"><img id="icon" src="twitter.png"></a></h1>

<?= $error ?>

<center><img src="curves/<?= $q ?>.png" title="<?= $q ?>"></center>

<?= equations($q, "tex") ?>

<h2>Archives</h2>

<?= archives() ?>

<br><br>

<center><i>Powered by <a href="http://www.sagemath.org/">SageMath</a></i></center>

</body>

</html>
