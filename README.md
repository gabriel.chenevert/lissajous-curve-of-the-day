# Lissajous Curve of the Day

Archives ici: https://krab.fr/lcd depuis le 2020/01/01

## code

* `lcd.sage`

needs: `apt install tk`

fragment qui génère image dans `output.png` (faut initialiser variables de date)

* `lcd.py`

needs: `python3-twitter`

`-p`: pretend n'envoie pas le tweet

crée `date.sage`, déplace le fichier à la bonne place (`curves`)

pour regénérer ancienne faut changer la date à la main

* `api_keys.py` avec les clés API (gitignore)

* dans cron


