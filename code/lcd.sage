g = gcd(m,d)
img = parametric_plot( (cos(y + m*x), sin(d*x)), (x,0,2*pi/g), plot_points=1000, figsize=7, thickness=3 )
img.axes(false)
save(img, "output.png")
