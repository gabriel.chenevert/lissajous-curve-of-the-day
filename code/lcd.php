<?php


// init

error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors", 1);
ini_set("default_charset", "UTF-8");

define("DIR", "curves");

$dates = Array();

foreach(scandir(DIR, 1) as $file) if ($file[0] != ".")
    $dates[] = explode(".", $file)[0];

$error = "";

if (isset($_GET["q"])) {
    if (in_array($_GET["q"], $dates)) {
        $q = $_GET["q"];
    } else {
        $error = "<div style=\"color: red\">No curve generated yet for this date; here is a random one:</div>";
        $q = $dates[array_rand($dates)];
    }
} else $q = $dates[0];

// functions

function equations($q, $format) {
    
    $date = explode("-", $q);
    $y = $date[0];
    $m = $date[1];
    $m = ($m[0] == "0") ? $m[1] : $m;
    $d = explode(".", $date[2])[0];
    $d = ($d[0] == "0") ? $d[1] : $d;
    
    $eqs = Array();
    
    $eqs[0] = "x(t) = cos($y + ";
    $eqs[0] .= ($m == "1") ? "" : "$m ";
    $eqs[0] .= "t)";
    
    $eqs[1] = "y(t) = sin(";
    $eqs[1] .= ($d == "1") ? "" : "$d ";
    $eqs[1] .= "t)";
    
    if ($format == "tex") {
    
        $res = "$$ \begin{cases} ";
        $res .= str_replace("cos", "\cos", $eqs[0]);
        $res .= " \\\\ ";
        $res .= str_replace("sin", "\sin", $eqs[1]);
        $res .= " \\end{cases} $$";
    
    }
    
    return $res;

}

function archives() {
    
    global $dates;
    
    foreach($dates as $curve)
	// if ($curve != date("Y-m-d"))
        echo "<a href=\"?q=$curve\"><img class=\"mini\" src=\"" . DIR . "/$curve.png\" title=\"$curve\"></img></a>";

}

?>
