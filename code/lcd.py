#!/usr/bin/python3

import datetime, os, twitter, argparse

from api_keys import *

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--pretend", action="store_true")
parser.add_argument("-d", "--date")
args = vars(parser.parse_args())

endl = "\n"

if args["date"]:

  tokens = args["date"].split("-")

  year = int(tokens[0])
  month = int(tokens[1])
  day = int(tokens[2])

else:

  date = datetime.datetime.now()

  year  = date.year
  month = date.month
  day   = date.day

filename = "%s-%02d-%02d" % (year,month,day)

print("Generating %s..." % filename)

txt  = "# variable initialization" + endl
txt += endl
txt += "y = %s" % year + endl
txt += "m = %s" % month + endl
txt += "d = %s" % day + endl

os.chdir("/var/www/lcd/code")

with open("lcd.sage") as f:
    txt += f.read()

with open(filename + ".sage", "w") as f:
    f.write(txt)

os.system("/opt/bin/sage %s.sage" % filename)
os.system("rm %s.sage*" % filename)
os.system("mv output.png ../curves/%s.png" % filename)

if not args["pretend"]:

  print("Sending tweet...")

  api = twitter.Api(consumer_key        = consumer_key,
                    consumer_secret     = consumer_secret,
                    access_token_key    = access_token_key,
                    access_token_secret = access_token_secret)

  msg  = "x(t) = cos(%s + " % year
  if month != 1: msg += str(month)
  msg += "t)\ny(t) = sin("
  if day != 1: msg += str(day)
  msg += "t)"

  api.PostUpdate(msg, media="../curves/%s.png" % filename)

print("...ok")
print()
